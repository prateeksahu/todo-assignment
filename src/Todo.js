import React,{Component} from 'react';

class Todo extends Component {
      state = {
          edit: false,
          id: null,
          mockData: [
        ],
        comp: "completed",
        notcomp: "not completed"
    }

    onSubmitHandle(event) {
        event.preventDefault();
        this.setState(
            {
                mockData: [...this.state.mockData,
                    {
                        id: Date.now(),
                        title: event.target.item.value,
                        done: false,
                        date: new Date()    
                    }
                ]  
                }
                );
                event.target.item.value = '';
            }
    onDeleteHandle(){
        let id = arguments[0];
        {console.log(id)}
        this.setState(
            {
                mockData: this.state.mockData.filter(
                    item => {
                        if(item.id !== id){
                            return item;
                        }
                    }
                )
            }
        );
    }

    onEditHandle(event){
        this.setState(
            {
                edit:true,
                id:arguments[0],
                title:arguments[1]
            }
        );
    }

    onCheck(){
        //event.preventDefault();
        let id = arguments[0];
        {console.log(id)}
        this.setState(
            {
                mockData: this.state.mockData.map(item => {
                    if(item.id === id)
                {
                    item['done']=!item['done']
                    console.log(item)
                    return item;
                }
                return item;
                }
                )
            }
        );
    }

    onUpdateHandle(event){
        event.preventDefault();
        this.setState(
            {
                mockData: this.state.mockData.map(item => { if(item.id === this.state.id)
                {
                    item['title'] = event.target.updatedItem.value;
                    return item;
                }
                return item;
            }
            )
            }
        );
        this.setState(
            {
                edit:false
            }
        );
    }

    renderEditForm() {
        if(this.state.edit) {
            return <form onSubmit={this.onUpdateHandle.bind(this)}>
            <div class="updateitem">
            <input type="text" name="updatedItem" className="item"/>
            <button className="update-add-item">Update</button>
            </div>
            </form>
        }
    }

    render(){
        return(
          <div>
            <div class="update">
              {this.renderEditForm()}
            </div>
            <form onSubmit={this.onSubmitHandle.bind(this)}>
            <div class="todoadd">
            <input type="text" name="item" className="item" class="item"/>
            <button className="btn-add-item" class="itemadd">Add</button>
            </div>
            </form>
            <div class="list">
            <table>
              {
                this.state.mockData.map(
                  item => (
                    <tr key={item.id}>
                      <td>{item.done? <strike>{item.title}</strike>:item.title}</td>
                      <td><button onClick={this.onDeleteHandle.bind(this, item.id)}>Delete</button></td>
                      <td><button onClick={this.onEditHandle.bind(this, item.id,item.title)}>Edit</button></td>
                        <td>{item.done ? this.state.comp : this.state.notcomp}</td>
                        <td><input type="checkbox" onClick={this.onCheck.bind(this,item.id)}/></td>
                    </tr>
                  )
                )
              }
            </table>
            </div>
          </div>
        );
      }
}

export default Todo;